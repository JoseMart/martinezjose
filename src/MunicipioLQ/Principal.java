package MunicipioLQ;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Principal {

    public static void main(String[] args) {

        List list = new List();
        Scanner leer = new Scanner(System.in);
        int op = 0;

        do {
            try {
                System.out.println("+-----------------------+\n"
                        + "|          Menu         |\n"
                        + "+-----------------------+\n"
                        + "| 1. Empleados          |\n"
                        + "| 2. Proyectos          |\n"
                        + "| 3. Salir              |\n"
                        + "+-----------------------+");
                System.out.print("Ingrese opcion: ");
                op = leer.nextInt();

                switch (op) {
                    case 1:
                        int op1 = 0;
                        do {
                            try {
                                System.out.println("+-----------------------+\n"
                                        + "|   SubMenu-Empleado    |\n"
                                        + "+-----------------------+\n"
                                        + "| 1. Agregar            |\n"
                                        + "| 2. Eliminar           |\n"
                                        + "| 3. Listar             |\n"
                                        + "| 4. Volver             |\n"
                                        + "+-----------------------+");
                                System.out.print("Ingrese opcion: ");
                                op1 = leer.nextInt();
                                switch (op1) {
                                    case 1:list.agregarEmp();
                                        break;
                                    case 2:list.eliminarEmp();
                                        break;
                                    case 3:list.listarEmp();
                                        break;
                                    case 4:
                                        break;
                                    default:
                                        System.out.println("¡Opcion No Encontrada!");
                                        break;
                                }
                            } catch (InputMismatchException im) {
                                System.out.println("¡Error! Ingrese Num Enteros ");
                                leer = new Scanner(System.in);
                            }
                        } while (op1 != 4);
                        break;
                    case 2:
                        int op2 = 0;
                        do {
                            try {
                                System.out.println("+--------------------------------+\n"
                                        + "|       SubMenu-Proyecto         |\n"
                                        + "+--------------------------------+\n"
                                        + "| 1. Agregar                     |\n"
                                        + "| 2. Eliminar                    |\n"
                                        + "| 3. Listar                      |\n"
                                        + "| 4. Agregar Emp a un Proyecto   |\n"
                                        + "| 5. Listar Datos de un Proyecto |\n"
                                        + "| 6. Calcular Total de Montos    |\n"
                                        + "| 7. Volver                      |\n"
                                        + "+--------------------------------+");
                                System.out.print("Ingrese opcion: ");
                                op2 = leer.nextInt();
                                switch (op2) {
                                    case 1:list.agregarPro();
                                        break;
                                    case 2:list.eliminarPro();
                                        break;
                                    case 3:list.listarPro();
                                        break;
                                    case 4:list.agregarEmpAProyecto();
                                        break;
                                    case 5:list.listarDatosDeProyecto();
                                        break;
                                    case 6:                                        
                                        float total=0;
                                        for (int i = 0; i < list.listpro.size(); i++) {
                                            total=list.listpro.get(i).getMonto()+total;
                                        }
                                        System.out.println("Total de monto destinado: "+total);
                                        break;
                                    case 7:
                                        break;
                                    default:
                                        System.out.println("¡Opcion No Encontrada!");
                                        break;
                                }
                            } catch (InputMismatchException im) {
                                System.out.println("¡Error! Ingrese Num Enteros ");
                                leer = new Scanner(System.in);
                            }
                        } while (op2 != 7);
                        break;
                    case 3:
                        System.out.println("Programa Finalizado");
                        break;
                    default:
                        System.out.println("¡Opcion No Encontrada!");
                        break;
                }
            } catch (InputMismatchException im) {
                System.out.println("¡Error! Ingrese Numeros Enteros ");
                leer = new Scanner(System.in);
            }
        } while (op != 3);

    }
}
