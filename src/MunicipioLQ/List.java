package MunicipioLQ;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class List {

    Scanner lectura = new Scanner(System.in);
    Empleado empleado = new Empleado();
    ArrayList<Empleado> listemp = new ArrayList();
    Proyecto proyecto = new Proyecto();
    ArrayList<Proyecto> listpro = new ArrayList();

    public void agregarEmp() {
        String opcion;
        try {
            do {

                System.out.print("Ingresar Dni: ");
                String dni = lectura.next();
                String ayn1 = lectura.nextLine();
                System.out.print("Ingrese AyN: ");
                String ayn = lectura.nextLine();
                System.out.print("Ingrese Telefono: ");
                int tel = lectura.nextInt();
                System.out.print("Ingrese Email: ");
                String em = lectura.next();

                empleado = new Empleado(dni, ayn, tel, em);
                listemp.add(empleado);

                System.out.print("Segir s/n: ");
                opcion = lectura.next();

            } while (opcion.equalsIgnoreCase("s"));
        } catch (Exception e) {
            System.out.println("¡Error! Ingrese numeros en Telefono");
            lectura = new Scanner(System.in);
        }
    }

    public void eliminarEmp() {
        String dni;
        boolean bandera = true;
        System.out.print("Eliminar Emp con Dni: ");
        dni = lectura.next();
        for (int i = 0; i < listemp.size(); i++) {
            if (listemp.get(i).getDni().equals(dni)) {
                listemp.remove(i);
                System.out.println("Se ELIMINO al Empleado con Dni " + dni);
                bandera = false;
            }
        }
        if (bandera == true) {
            System.out.println("No se encotro al Empleado con Dni " + dni + " solicitado");
        }
    }

    public void listarEmp() {
        if (listemp.isEmpty()) {
            System.out.println("Lista Vacia");
        } else {
            System.out.println("+-------------------------------------------------------------------------------+");
            System.out.println("|                                   Empleados                                   |");
            System.out.println("+-------------------------------------------------------------------------------+");
            System.out.println("|       DNI         | Apellido y Nombre |     Telefono      |     Email         |");
            System.out.println("+-------------------+-------------------+-------------------+-------------------+");
            Collections.sort(listemp);
            for (int i = 0; i < listemp.size(); i++) {
                listemp.get(i).mostrarEmp();
            }
            System.out.println("+-------------------------------------------------------------------------------+");
            System.out.printf("|%-79s|\n", "Total de Empleados: " + listemp.size());
            System.out.println("+-------------------------------------------------------------------------------+");
        }
    }

    public void agregarPro() {
        String opcion;
        boolean ban = false;
        try {
            do {

                System.out.print("Ingresar Id: ");
                int id = lectura.nextInt();
                String nom1 = lectura.nextLine();
                System.out.print("Ingrese Nombre del Proyecto: ");
                String nom = lectura.nextLine();

                System.out.print("Ingrese Monto Destinado: ");
                float monto = lectura.nextFloat();

                proyecto = new Proyecto(id, nom, monto);
                listpro.add(proyecto);

                System.out.print("Segir s/n: ");
                opcion = lectura.next();

            } while (opcion.equalsIgnoreCase("s"));
        } catch (Exception e) {
            System.out.println("¡Error! INGRESE(numeros en Id y Monto, coma en Monto)");
            lectura = new Scanner(System.in);
        }
    }

    public void eliminarPro() {
        int id;
        boolean bandera = true;
        System.out.print("Eliminar Proyecto con Id: ");
        id = lectura.nextInt();
        for (int i = 0; i < listpro.size(); i++) {
            if (listpro.get(i).getId() == id) {
                listpro.remove(i);
                System.out.println("Se ELIMINO el Proyecto con Id " + id);
                bandera = false;
            }
        }
        if (bandera == true) {
            System.out.println("No se encotro El Proyecto con Id " + id + " solicitado");
        }
    }

    public void listarPro() {
        if (listpro.isEmpty()) {
            System.out.println("Lista Vacia");
        } else {
            System.out.println("+-----------------------------------------------------------+");
            System.out.println("|                        Proyectos                          |");
            System.out.println("+-----------------------------------------------------------+");
            System.out.println("|     ID      |  Nombre Proyecto        |      Monto        |");
            System.out.println("+-------------+-------------------------+-------------------+");
            for (int i = 0; i < listpro.size(); i++) {
                listpro.get(i).mostrarProyecto();
            }
            System.out.println("+-----------------------------------------------------------+");
        }
    }

    public void agregarEmpAProyecto() {
        if (listpro.isEmpty()) {
            System.out.println("No hay ningun Proyecto");
        } else {
            System.out.println("+-----------------------------------------------------------+");
            System.out.println("|     ID      |  Nombre Proyecto        |      Monto        |");
            System.out.println("+-------------+-------------------------+-------------------+");
            for (int i = 0; i < listpro.size(); i++) {
                listpro.get(i).mostrarProyecto();
            }
            System.out.println("+-----------------------------------------------------------+");
            System.out.print("Eliga un Proyecto (Id Proyecto): ");
            int id = lectura.nextInt();
            if (listemp.isEmpty()) {
                System.out.println("No hay ningun Empleado para Agregar");
            } else {
                System.out.println("+---------------------------------------+");
                System.out.println("|        N°         | Apellido y Nombre |");
                System.out.println("+-------------------+-------------------+");
                Collections.sort(listemp);
                for (int i = 0; i < listemp.size(); i++) {
                    listemp.get(i).mostrarEmp2(i);
                }
                System.out.println("+---------------------------------------+");
                System.out.print("Seleccione N° del Empleado a Agregar: ");
                int num = lectura.nextInt();

                Empleado empaux;
                if (num < listemp.size()) {
                    empaux = new Empleado(listemp.get(num).getDni(), listemp.get(num).getApeynom(), listemp.get(num).getTelefono(), listemp.get(num).getEmail());
                    for (int j = 0; j < listpro.size(); j++) {
                        if (listpro.get(j).getId() == id) {
                            listpro.get(j).addEmpAProyecto(empaux);
                            System.out.println("Se agrego");
                        }
                    }

                } else {
                    System.out.println("No existe el Empleado");
                }
            }
        }

    }



    public void listarDatosDeProyecto() {
        if (listpro.isEmpty()) {
            System.out.println("Esta vacia la lista");
        } else {
            System.out.println("+-----------------------------------------------------------+");
            System.out.println("|     ID      |  Nombre Proyecto        |      Monto        |");
            System.out.println("+-------------+-------------------------+-------------------+");
            for (int i = 0; i < listpro.size(); i++) {
                listpro.get(i).mostrarProyecto();
            }
            System.out.println("+-----------------------------------------------------------+");
            System.out.print("Ingrese Id del Proyecto: ");
            int id = lectura.nextInt();
            System.out.println("+-------------------------------------------------------------------------------+");
            System.out.println("|                                  Proyecto                                     |");
            System.out.println("|        ID         |  Nombre Proyecto                      |      Monto        |");
            for (int i = 0; i < listpro.size(); i++) {
                if (listpro.get(i).getId() == id) {
                    listpro.get(i).mostrarProyecto2();
                }
            }
            System.out.println("+-------------------+---------------------------------------+-------------------+");
            System.out.println("|                               Participantes                                   |");
            System.out.println("|       DNI         | Apellido y Nombre |     Telefono      |     Email         |");
            for (int i = 0; i < proyecto.listempagregados.size(); i++) {
                proyecto.listempagregados.get(i).mostrarEmp();
            }
            System.out.println("+-------------------------------------------------------------------------------+");
        }
    }

}
