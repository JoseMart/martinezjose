package MunicipioLQ;

public class Empleado implements Comparable<Empleado>{
    
    private String dni;
    private String apeynom;
    private int telefono;
    private String email;

    public Empleado(){}
    public Empleado(String dni, String apeynom, int telefono, String email) {
        this.dni = dni;
        this.apeynom = apeynom;
        this.telefono = telefono;
        this.email = email;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getApeynom() {
        return apeynom;
    }

    public void setApeynom(String apeynom) {
        this.apeynom = apeynom;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    
    
    public void mostrarEmp(){
        System.out.printf("| %-18s| %-18s| %-18s| %-18s|\n",dni,apeynom,telefono,email);
    }
    public void mostrarEmp2(int num){
        System.out.printf("| %-18s| %-18s|\n",num,apeynom);
    }

    @Override
    public int compareTo(Empleado e) {
        return apeynom.compareToIgnoreCase(e.getApeynom());
    }
    
}
