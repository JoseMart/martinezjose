package MunicipioLQ;

import java.util.ArrayList;

public class Proyecto {
    
    private int id;
    private String nom;
    private float monto;
    private Empleado emp=new Empleado();
    ArrayList<Empleado> listempagregados=new ArrayList();

    public Proyecto(){}
    public Proyecto(int id, String nom, float monto) {
        this.id = id;
        this.nom = nom;
        this.monto = monto;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public float getMonto() {
        return monto;
    }

    public void setMonto(float monto) {
        this.monto = monto;
    }    
    
    
    public void mostrarProyecto(){
        System.out.printf("| %-12s| %-24s| %-18s|\n",id,nom,monto);
    }
    public void mostrarProyecto2(){
        System.out.printf("| %-18s| %-38s| %-18s|\n",id,nom,monto);
    }
    
    public void addEmpAProyecto(Empleado e){
        listempagregados.add(e);
    }
    

}
